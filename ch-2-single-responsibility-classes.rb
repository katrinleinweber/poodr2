class Gear
  attr_reader :chainring, :cog, :wheel
  def initialize(chainring, cog, wheel = nil)
    @chainring = chainring
    @cog       = cog
    @wheel     = wheel
  end

  def gear_inches
    ratio * wheel.diameter
  end

  def ratio
    chainring / cog.to_f
    # Using @instance @variables here would be "road to ruin".
    # Let attr_* methods create wrappers for attributes!
    # Enables explicit implementation later […], e.g. with
    # unanticipated_adjustment_factor
  end

  # […] where, instance variables would be OK, because "Omega Mess", see
  # https://www.youtube.com/watch?v=mpA2F1In41w
  #
  # def cog
  #   @cog * (foo? ? bar_adjustment : baz_adjustment)
  # end
end

class Wheel
  attr_reader :rim, :tire

  def initialize(rim, tire)
    @rim       = rim
    @tire      = tire
  end

  def diameter
    rim + (tire * 2)
  end

  def circumference
    diameter * Math::PI
  end
end

class RevealingReferences
  attr_reader :wheels
  def initialize(data)
    @wheels = wheelify(data)
  end

  def diameters
    wheels.collect { |wheel|
      wheel.rim + (wheel.tire * 2) }
  end

  # now everyone can send rim/tire to wheel
  Wheel = Struct.new(:rim, :tire)
  def wheelify(data)
    data.collect { |cell|
      Wheel.new(cell[0], cell[1]) }
  end
end
