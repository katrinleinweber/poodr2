class Gear
  attr_reader :chainring, :cog, :wheel
                               # Still called like this, but only internally,
                               # thus externally uncoupled.
  def initialize(chainring: 52,  wheel:,
                 cog:       default_cog)
                          # Defaults aren't possible with
                          # initialize(args)
                          # @… = args[:…]
    @chainring = chainring
    @cog       = cog
    @wheel     = wheel
  end

  def default_cog
    1 + 2 * 3
  end

  def gear_inches
    ratio * diameter
  end

  def diameter
    wheel.diameter
  end

  def ratio
    chainring / cog.to_f
  end
end

class AnyClass
  # Can be injected into Gear now.
  # Gear.new(wheel: AnyClass.new(26, 1.5)).gear_inches
  attr_reader :rim, :tire
  def initialize(rim, tire)
    @rim       = rim
    @tire      = tire
  end

  def diameter
    rim + (tire * 2)
  end
end
