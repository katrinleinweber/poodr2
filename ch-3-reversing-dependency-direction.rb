class Gear
  # Reverse injection direction now:
  # AnyClass.new(rim: 26, tire: 1.5).gear_inches
  attr_reader :chainring, :cog
  def initialize(chainring:, cog:)
    @chainring = chainring
    @cog       = cog
  end

  def gear_inches(diameter)
    ratio * diameter
  end

  def ratio
    chainring / cog.to_f
  end
end

class AnyClass
  attr_reader :rim, :tire, :gear
  def initialize(rim:, tire:, chainring: 52, cog: default_cog)
    @rim       = rim
    @tire      = tire
    @gear      = Gear.new(chainring: chainring, cog: cog)
  end

  def default_cog
    1 + 2 * 3
  end

  def gear_inches

       # Method from injected class
    gear.gear_inches(diameter)
                   # method from AnyClass
  end

  def diameter
    rim + (tire * 2)
  end
end
