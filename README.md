# poodr2

A fork of [GitHub.com/skmetz/poodr2](https://github.com/skmetz/poodr2) with summaries / learnings in the commit messages.

That way, the `git blame` view of a file, and the [commits list](https://gitlab.com/katrinleinweber/poodr2/-/commits/main) should be useful for reviewing, recalling and learning.

Context: [gitlab-com/book-clubs#29](https://gitlab.com/gitlab-com/book-clubs/-/issues/29 "Practical Object-Oriented Design")

[Read a sample of the book and buy it](www.sandimetz.com/products#product-poodr). It is very good.
