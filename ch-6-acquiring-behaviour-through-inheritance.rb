class Bicycle
  attr_reader :size, :chain, :tire_size

  def initialize(**opts)
    @size      = opts[:size]
    @chain     = opts[:chain]     || default_chain
    @tire_size = opts[:tire_size] || default_tire_size
    post_initialize(opts)
  end

  def spares
    { chain:     chain,
      tire_size: tire_size }
    .merge(local_spares)
  end

  def default_tire_size
    raise NotImplementedError,
          "#{self.class} should have implemented..."
  end

  def post_initialize(opts)
  end

  def local_spares = {}
  def default_chain = "11-speed"
end

class RoadBike < Bicycle
  attr_reader :tape_color

  def post_initialize(opts)
    @tape_color = opts[:tape_color]
  end

  def local_spares = { tape_color: tape_color }
  def default_tire_size = "23"
end

class MountainBike < Bicycle
  attr_reader :front_shock, :rear_shock

  def post_initialize(opts)
    @front_shock = opts[:front_shock]
    @rear_shock  = opts[:rear_shock]
  end

  def local_spares = { front_shock: front_shock }
  def default_tire_size = "2.1"
end

class RecumbentBike < Bicycle
  attr_reader :flag

  def post_initialize(opts)
    @flag = opts[:flag]
  end

  def local_spares = { flag: flag }
  def default_chain = '10-speed'
  def default_tire_size = '28'
end
