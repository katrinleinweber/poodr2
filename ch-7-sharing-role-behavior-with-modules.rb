class Schedule
  def scheduled?(schedulable, starting, ending)
    puts "This #{schedulable.class} is " +
         "available #{starting} - #{ending}"
    false
  end

  def add(target, starting, ending)
  end

  def remove(target, starting, ending)
  end
end


module Schedulable
  attr_writer :schedule

  def schedule
    @schedule ||= Schedule.new
  end

  def schedulable?(starting, ending)
    !scheduled?(starting - lead_days, ending)
  end

  def scheduled?(starting, ending)
    schedule.scheduled?(self, starting, ending)
  end

  def lead_days = 0
end


class Bicycle
  include Schedulable

  def lead_days = 1
end

class Vehicle
  include Schedulable

  def lead_days = 3
end

class Mechanic
  include Schedulable

  def lead_days = 4
end
