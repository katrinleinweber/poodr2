class AnyClass
  # Can be injected into GearWrapper.
  # GearWrapper.gear(wheel: AnyClass.new(26, 1.5)).gear_inches
  attr_reader :rim, :tire
  def initialize(rim, tire)
    @rim = rim
    @tire = tire
  end

  def diameter
    rim + (tire * 2)
  end
end

module GearWrapper
  def self.gear(chainring: 52, cog: default_cog, wheel:)
    ExternalFramework::Gear.new(wheel, cog, chainring)
  end

  def self.default_cog
    1 + 2 * 3
  end
end

module ExternalFramework
  class Gear
    attr_reader :chainring, :cog, :wheel
    def initialize(wheel, cog, chainring)
      @wheel = wheel
      @cog = cog
      @chainring = chainring
    end

    def gear_inches
      ratio * wheel.diameter
    end

    def ratio
      chainring / cog.to_f
    end
  end
end
